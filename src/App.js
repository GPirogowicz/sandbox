import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import './common/assets/styles/common.scss';
import Layout from './common/components/Layout/Layout';
import { routes } from './common/helpers/Routes';
import RouteWrapper from './common/helpers/Routes/RouteWrapper/RouteWrapper';

const App = () => {
    return (
        <>
            <BrowserRouter>
                <Switch>
                    {Array.isArray(routes) &&
                        routes.map((routeProps, key) => (
                            <RouteWrapper
                                {...routeProps}
                                key={key}
                                layout={Layout}
                            />
                        ))}
                </Switch>
            </BrowserRouter>
        </>
    );
};

export default App;
export { App };
