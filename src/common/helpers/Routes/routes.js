import { Home } from '../../pages/Home';

const routes = [
    {
        path: '/',
        component: Home,
        exact: false,
    },
];

export default routes;
