import React from 'react';
import { Route } from 'react-router-dom';

import PropTypes from 'prop-types';

const RouteWrapper = ({ component: Component, layout: Layout, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props => (
                <Layout {...props}>
                    <Component {...props} />
                </Layout>
            )}
        />
    );
};

RouteWrapper.propTypes = {
    component: PropTypes.oneOfType([
        Route.propTypes.component,
        PropTypes.object,
    ]),
    layout: PropTypes.func,
};

export default RouteWrapper;
