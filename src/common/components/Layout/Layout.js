import React from 'react';
import PropTypes from 'prop-types';
import './layout.scss';
const Layout = ({ children }) => {
    return (
        <div className={'o-layout'}>
            <div className={'o-layout__sidebar'}>Sidebar</div>
            <div className={'o-layout__content'}>{children}</div>
        </div>
    );
};

Layout.propTypes = {
    children: PropTypes.element,
};

export default Layout;
