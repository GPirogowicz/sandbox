const webpack = require('webpack');
const path = require('path');
require('dotenv').config({
    path: path.resolve(__dirname, '../.env.local'),
});

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const DotenvPlugin = require('dotenv-webpack');
// const CopyPlugin = require('copy-webpack-plugin');
const LoadablePlugin = require('@loadable/webpack-plugin');
const isEnvProduction =
    JSON.stringify(process.env.NODE_ENV) === JSON.stringify('production');
const commonConfig = {
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash].css',
            chunkFilename: 'css/[name].[contenthash].css',
        }),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /pl/),
        new DotenvPlugin({
            path: path.resolve(__dirname, '../.env.local'),
            safe: true,
            systemvars: true,
            silent: true,
        }),
        new LoadablePlugin(),
        // new BundleAnalyzerPlugin()
    ],
    resolve: {
        extensions: [
            '*',
            '.web.mjs',
            '.mjs',
            '.web.js',
            '.js',
            '.web.ts',
            '.ts',
            '.d.ts',
            '.web.tsx',
            '.tsx',
            '.json',
            '.web.jsx',
            '.jsx',
        ],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader'],
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: isEnvProduction
                            ? MiniCssExtractPlugin.loader
                            : 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: 'config',
                            },
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass'),
                        },
                    },
                ],
            },
            {
                test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'media/[name].[hash:8].[ext]',
                },
            },
            {
                test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name]_[hash:4].[ext]',
                        },
                    },
                ],
            },
        ],
    },
};

const clientConfig = {
    ...commonConfig,
};
module.exports = clientConfig;
